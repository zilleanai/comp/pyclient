import inspect
from flask_unchained import AppFactoryHook, FlaskUnchained
from typing import *


class DiscoverPyClientHook(AppFactoryHook):
    """
    Discovers celery tasks.
    """
    name = 'pyclient'
    bundle_module_name = 'pyclient'


    def process_objects(self, app: FlaskUnchained, objects: Dict[str, Any]):
        for name, module in objects.items():
            print('bundle', name)
            self.bundle.bundles[name] = module

    def type_check(self, obj):
        if not inspect.isclass(obj):
            return False
        return True
