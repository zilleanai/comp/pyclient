from flask_unchained import Bundle

class PyClientBundle(Bundle):
    bundles = {}
    name = 'pyclient_bundle'
    """
    The :class:`Bundle` subclass for the pyclient bundle. Has no special behavior.
    """
    command_group_names = ['pyclient']
