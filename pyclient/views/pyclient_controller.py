import os
import sys
import json
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable, bundles
from backend.config import Config as AppConfig

from werkzeug.utils import secure_filename

class PyClientController(Controller):

    @route('/<string:module>')
    def module(self, module):
        print(module)
        if module:
            m = current_app.unchained.pyclient_bundle.bundles[module].__module__
            return send_file(sys.modules[m].__file__)
        modules = []
        for bundle in current_app.unchained.pyclient_bundle.bundles:
            module = current_app.unchained.pyclient_bundle.bundles[bundle].__module__
            print(sys.modules[module].__file__)
            modules.append(bundle)
        resp = jsonify(modules=modules)
        return resp

    @route('/')
    def modules(self):
        modules = []
        for bundle in current_app.unchained.pyclient_bundle.bundles:
            module = current_app.unchained.pyclient_bundle.bundles[bundle].__module__
            print(sys.modules[module].__file__)
            modules.append(bundle)
        resp = jsonify(modules=modules)
        return resp
